package com.maapza.maapza.data.remote.model;

import android.os.Parcel;

import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by LUIS.RODARTE on 7/16/18.
 */

@IgnoreExtraProperties
public class CategorySuggestion implements SearchSuggestion {

    private String id_category;
    private String name;
    private String icon;

    public CategorySuggestion(){}

    public CategorySuggestion(String id_category, String name, String icon) {
        this.id_category = id_category;
        this.name = name.toLowerCase();
        this.icon = icon;
    }

    public CategorySuggestion(Parcel in) {
        this.id_category = in.readString();
        this.name = in.readString();
        this.icon = in.readString();
    }


    @Override
    public String getBody() {
        return name;
    }


    public static final Creator<CategorySuggestion> CREATOR = new Creator<CategorySuggestion>() {
        @Override
        public CategorySuggestion createFromParcel(Parcel in) {
            return new CategorySuggestion(in);
        }

        @Override
        public CategorySuggestion[] newArray(int size) {
            return new CategorySuggestion[size];
        }
    };

    public String getId_category() {
        return id_category;
    }

    public void setId_category(String id_category) {
        this.id_category = id_category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id_category);
        dest.writeString(name);
        dest.writeString(icon);
    }
}