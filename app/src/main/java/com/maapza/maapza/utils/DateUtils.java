package com.maapza.maapza.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by LUIS.RODARTE on 23/05/2018.
 */

public class DateUtils {

    public String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }

    public static String getCurrentTime() {
        DateFormat dateFormat = new SimpleDateFormat("h:mm aa", Locale.ENGLISH);
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }

    public static int getCurrentDay(){

        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    public static boolean isAvailable(String openTime, String closedTime) {
        boolean response = false;
        DateFormat dateFormat = new SimpleDateFormat("h:mm aa", Locale.ENGLISH);
        try {
            Date open = dateFormat.parse(openTime);
            Date closed = dateFormat.parse(closedTime);
            Date current = dateFormat.parse(getCurrentTime());
            if (current.after(open) && current.before(closed)) {
                response = true;
            } else {
                response = false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
            response = false;
        }
        return response;
    }
}
