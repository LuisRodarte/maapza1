package com.maapza.maapza.ui.placedetail;

import com.maapza.maapza.data.remote.model.Comment;
import com.maapza.maapza.data.remote.model.Place;
import com.maapza.maapza.data.remote.model.PlacePhotos;
import com.maapza.maapza.data.remote.model.Times;

import java.util.List;

public class PlacePresenterImpl implements PlaceContract.Presenter, PlaceContract.Interactor.OnFinishedListener {

    private PlaceContract.View view;
    private PlaceContract.Interactor interactor;

    public PlacePresenterImpl(PlaceContract.View view) {
        this.view = view;
        interactor = new PlaceInteractorImpl();
    }

    @Override
    public void getPlacesbyId(String place_id) {
        view.showLoading(true);
        interactor.doGetPlacesbyId(this, place_id);

    }

    @Override
    public void getPhotosbyIdPlace(String place_id) {
        view.showLoading(true);
        interactor.doGetPhotosbyIdPlace(this, place_id);
    }

    @Override
    public void getTimesByIdPlace(String place_id) {
        view.showLoading(true);
        interactor.doGetTimesByIdPlace(this, place_id);
    }

    @Override
    public void getCommentsByIdPlace(String place_id) {
        view.showLoading(true);
        interactor.doGetCommentsByIdPlace(this, place_id);
    }

    @Override
    public void sendReview(String place_id, String comments, Float rating, String date) {
        view.showLoading(true);
        interactor.doSendReview(this, place_id, comments, rating, date);
    }

    @Override
    public void onFinishedGetPlaces(Place place) {
        view.showLoading(false);
        view.displayPlaceData(place);

    }

    @Override
    public void onFinishedGetPhotos(List<PlacePhotos> listPhotos) {
        view.showLoading(false);
        view.displayPhotos(listPhotos);
    }

    @Override
    public void onFinishedGetTimes(List<Times> timesList) {
        view.showLoading(false);
        view.displayTimes(timesList);
    }

    @Override
    public void onFinishedGetComments(List<Comment> listComments) {
        view.showLoading(false);
        view.displayComments(listComments);
    }

    @Override
    public void onErrorGetData() {
        view.showLoading(false);
        view.showErrorMessage();
    }

    @Override
    public void onFinishedSendReview() {
        view.showLoading(false);
        view.showReviewSuccess();

    }

    @Override
    public void onErrorSendReview() {
        view.showLoading(false);
        view.showErrorSendingReview();
    }
}
