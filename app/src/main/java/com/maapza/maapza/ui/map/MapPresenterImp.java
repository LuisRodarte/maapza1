package com.maapza.maapza.ui.map;

import com.maapza.maapza.data.remote.model.Category;
import com.maapza.maapza.data.remote.model.CategorySuggestion;
import com.maapza.maapza.data.remote.model.Place;

import java.util.List;

public class MapPresenterImp implements MapContract.Presenter, MapContract.Interactor.OnFinishedListener {
    private MapContract.View view;
    private MapContract.Interactor interactor;


    public MapPresenterImp(MapContract.View view) {
        this.view = view;
        interactor = new MapInteractorImp();
    }

    @Override
    public void getCategories() {
        //view.showLoading(true);
        interactor.doGetCategories(this);
    }

    @Override
    public void getPlacesById(String place_id) {
        view.showLoading(true);
        interactor.doGetPlaceById(this, place_id);

    }

    @Override
    public void getPlacesByCategory(String id_category) {
        view.showLoading(true);
        interactor.doGetPlacesByCategories(this, id_category);
    }

    @Override
    public void getAllPlaces() {
        view.showLoading(true);
        interactor.doGetAllPlaces(this);
    }


    @Override
    public void onFinishedGetCategories(List<CategorySuggestion> suggestions) {
        //view.showLoading(false);
        view.displayCategories(suggestions);

    }

    @Override
    public void onFinishedGetPlaceById(Place place) {
        view.showLoading(false);
        view.displayPlaceData(place);
    }

    @Override
    public void onFinishedGetPlacesByCategory(List<Place> placeList) {
        view.showLoading(false);
        view.displayPlaces(placeList);
    }

    @Override
    public void onErrorGetData() {
        view.showLoading(false);
        view.showErrorMessage();
    }

    @Override
    public void onPlaceNotFound() {
        view.showLoading(false);
        view.showMessageNoPlaceFound();
    }


}
