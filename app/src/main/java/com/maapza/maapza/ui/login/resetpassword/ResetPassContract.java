package com.maapza.maapza.ui.login.resetpassword;

public interface ResetPassContract {

    interface View {

        void showResetErrorMessage();

        void showLoading(Boolean show);

        void closeActivity();

        void resetSuccess();
    }

    interface Presenter {
        void resetPassword(String email);
    }

    interface Interactor {

        interface OnFinishedListener {
            void onFinishedResetPass();

            void onErrorResetPass();

            void onFailure(Throwable t);
        }

        void doResetPass(ResetPassContract.Interactor.OnFinishedListener onFinishedListener, String email);

    }
}
