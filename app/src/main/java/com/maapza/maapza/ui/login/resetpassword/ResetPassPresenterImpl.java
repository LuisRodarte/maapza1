package com.maapza.maapza.ui.login.resetpassword;

public class ResetPassPresenterImpl implements ResetPassContract.Presenter, ResetPassContract.Interactor.OnFinishedListener {

    private ResetPassContract.View view;
    private ResetPassContract.Interactor interactor;

    public ResetPassPresenterImpl(ResetPassContract.View view) {
        this.view = view;
        interactor = new ResetPassInteractorImpl();
    }

    @Override
    public void resetPassword(String email) {
        view.showLoading(true);
        interactor.doResetPass(this, email);

    }

    @Override
    public void onFinishedResetPass() {
        view.showLoading(false);
        view.resetSuccess();
    }

    @Override
    public void onErrorResetPass() {
        view.showLoading(false);
        view.showResetErrorMessage();
    }

    @Override
    public void onFailure(Throwable t) {
        view.showLoading(false);
        view.showResetErrorMessage();
    }
}
