package com.maapza.maapza.ui.login.signin;

import com.google.firebase.auth.FirebaseAuth;

public class SigninIteractorImpl implements SigninContract.Interactor {
    private FirebaseAuth auth;

    public SigninIteractorImpl() {
        auth = FirebaseAuth.getInstance();
    }

    @Override
    public void checkSession(OnFinishedListener onFinishedListener) {
        onFinishedListener.onFinished(auth.getCurrentUser());
    }
}
