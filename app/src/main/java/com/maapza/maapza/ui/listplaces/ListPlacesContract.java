package com.maapza.maapza.ui.listplaces;

import com.maapza.maapza.data.remote.model.Place;

import java.util.List;

public interface ListPlacesContract {

    interface View {

        void showErrorGetPlaces();

        void showLoading(Boolean show);

        void closeActivity();

        void displayListPlaces(List<Place> placeList);
    }

    interface Presenter {
        void getListPlaces();
    }

    interface Interactor {

        interface OnFinishedListener {
            void onFinishedGetPlaces(List<Place> placeList);

            void onErrorGetPlaces();

            void onFailure(Throwable t);
        }

        void doGetPlaces(ListPlacesContract.Interactor.OnFinishedListener onFinishedListener);

    }
}
