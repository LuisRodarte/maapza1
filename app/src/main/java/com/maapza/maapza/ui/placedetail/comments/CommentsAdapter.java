package com.maapza.maapza.ui.placedetail.comments;

import android.content.Context;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.maapza.maapza.R;
import com.maapza.maapza.data.remote.model.Comment;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by LUIS.RODARTE on 22/05/2018.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.MyViewHolder> {

    private List<Comment> commentsList;
    Typeface face;
    private Context mContext;
    private ClickHandler clickHandler;



    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.textComment)
        TextView textComment;
        @BindView(R.id.textDate)
        TextView txtDate;
        @BindView(R.id.ratingBar)
        RatingBar ratingBar;
        @BindView(R.id.imageUser)
        CircularImageView imageUser;
        @BindView(R.id.container)
        ConstraintLayout container;


        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }


    public CommentsAdapter(Context mContext,List<Comment> commentsList, ClickHandler clickHandler) {
        this.mContext = mContext;
        this.commentsList = commentsList;
        this.clickHandler=clickHandler;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_list_row, parent, false);
        face = Typeface.createFromAsset(mContext.getAssets(), "quicksand.ttf");
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        final Comment comment = commentsList.get(position);
        holder.txtName.setTypeface(face);
        holder.txtDate.setTypeface(face);
        holder.textComment.setTypeface(face);
        holder.txtName.setText(comment.getName_user());
        holder.txtDate.setText(comment.getComment_date());
        if(comment.getComment().length()>150){
            holder.textComment.setText(comment.getComment().substring(0,150)+" ...");
        }else{
            holder.textComment.setText(comment.getComment());
        }
        holder.ratingBar.setRating(comment.getRating());
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.alpha_gradient);
        requestOptions.error(R.drawable.alpha_gradient4);
        requestOptions.centerCrop();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
        requestOptions .override(100, 100);
        Glide.with(mContext)
                .load(comment.getUrl_photo())
               .apply(requestOptions)
                .into(holder.imageUser);
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickHandler.onClickComment(comment);
            }
        });

    }
    @Override
    public int getItemCount() {
        return commentsList.size();
    }

    public interface ClickHandler{
         void onClickComment(Comment comment);

    }
}
