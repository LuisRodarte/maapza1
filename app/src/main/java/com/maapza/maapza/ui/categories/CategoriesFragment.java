package com.maapza.maapza.ui.categories;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.maapza.maapza.R;
import com.maapza.maapza.data.remote.model.Category;
import com.maapza.maapza.ui.map.MapFragment;

import java.util.List;

import static com.maapza.maapza.common.Constants.BUNDLE_ID_CATEGORY;
import static com.maapza.maapza.common.Constants.BUNDLE_NAME_CATEGORY;

public class CategoriesFragment extends Fragment implements CategoriesContract.View, CategoriesAdapter.ClickHandler {


    private RecyclerView recyclerView;
    GridLayoutManager layoutManager;
    private View myFragmentView;
    private CategoriesContract.Presenter presenter;
    private ProgressBar progressBar;

    public static CategoriesFragment newInstance() {
        CategoriesFragment fragment = new CategoriesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_categories, container, false);
        presenter = new CategoriesPresenterImpl(this);
        setUI();
        presenter.getListCategories();
        return myFragmentView;

    }

    private void setUI() {
        recyclerView = myFragmentView.findViewById(R.id.recycleViewCategories);
        progressBar= myFragmentView.findViewById(R.id.progressbar);
        layoutManager = new GridLayoutManager(myFragmentView.getContext(), 3);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(),message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(getContext(), getString(R.string.sesion_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(Boolean show) {
        int stateLoader = show ? View.VISIBLE : View.GONE;
        progressBar.setVisibility(stateLoader);
    }

    @Override
    public void closeActivity() {
        getActivity().finish();
    }
    @Override
    public void displayListCategories(List<Category> categories) {
        CategoriesAdapter adapter = new CategoriesAdapter(getContext(), categories, 1, this);
        recyclerView.setAdapter(adapter);
    }

    private void navToMap(String idCategory, String nameCategory) {
        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_ID_CATEGORY, idCategory);
        bundle.putString(BUNDLE_NAME_CATEGORY, nameCategory);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MapFragment mapFragment = new MapFragment();
        mapFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.frame_layout, mapFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void OnclickItem(String idCategory, String name) {
        navToMap(idCategory, name);
    }
}