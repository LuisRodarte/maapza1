package com.maapza.maapza.ui.listplaces;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.maapza.maapza.R;
import com.maapza.maapza.data.remote.model.Place;
import com.maapza.maapza.ui.placedetail.PlaceFragment;

import java.util.List;

import static com.maapza.maapza.common.Constants.BUNDLE_ID_PLACE;

/**
 * Created by MacHD on 11/4/18.
 */

public class ListPlacesFragment extends Fragment implements ListPlacesContract.View, PlaceAdapter.ClickHandler {
    private View myFragmentView;
    private RecyclerView recyclerViewPlace;
    private ProgressBar progressBar;
    private ListPlacesContract.Presenter presenter;


    public static ListPlacesFragment newInstance() {
        ListPlacesFragment fragment = new ListPlacesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_list_places, container, false);
        presenter = new ListPlacesPresenterImpl(this);
        setUI();
        presenter.getListPlaces();
        return myFragmentView;

    }

    private void setUI() {

        recyclerViewPlace = myFragmentView.findViewById(R.id.rvPlaces);
        progressBar = myFragmentView.findViewById(R.id.progressbar);
        TextView tvTitle = myFragmentView.findViewById(R.id.tvTitle);
        Typeface face;
        face = Typeface.createFromAsset(getContext().getAssets(), "quicksand.ttf");
        tvTitle.setTypeface(face);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerViewPlace.setItemAnimator(new DefaultItemAnimator());
        recyclerViewPlace.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        recyclerViewPlace.setLayoutManager(mLayoutManager);

    }


    @Override
    public void showErrorGetPlaces() {
        Toast.makeText(getContext(), getString(R.string.sesion_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(Boolean show) {
        int stateLoader = show ? View.VISIBLE : View.GONE;
        progressBar.setVisibility(stateLoader);
    }

    @Override
    public void closeActivity() {
        getActivity().finish();
    }

    @Override
    public void displayListPlaces(List<Place> placeList) {
        PlaceAdapter adapterPlace = new PlaceAdapter(getContext(), placeList, "all", this);
        recyclerViewPlace.setAdapter(adapterPlace);
    }

    @Override
    public void onClickPlace(String idPlace) {

        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_ID_PLACE, idPlace);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        PlaceFragment placeFragment = new PlaceFragment();
        placeFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.frame_layout, placeFragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack(placeFragment.getTag());
        fragmentTransaction.commit();
    }

    @Override
    public void navToListPlaces() {

    }
}
