package com.maapza.maapza.ui.categories;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.maapza.maapza.data.remote.model.Category;

import java.util.ArrayList;
import java.util.List;

import static com.maapza.maapza.data.remote.FirebaseEndPoints.CATEGORY_ENDPOINT;

public class CategoriesIteractorimpl implements CategoriesContract.Interactor {

    private DatabaseReference categories;

    public CategoriesIteractorimpl() {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        categories = database.getReference(CATEGORY_ENDPOINT);
    }

    @Override
    public void doGetCategories(final OnFinishedListener onFinishedListener) {

        categories.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Category> categoriesList = new ArrayList<>();
                for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                    Category category = categorySnapshot.getValue(Category.class);
                    categoriesList.add(category);
                }
                onFinishedListener.onFinishedGetCategories(categoriesList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
              //  onFinishedListener.onErrorGetCategories();
            }
        });


    }
}
