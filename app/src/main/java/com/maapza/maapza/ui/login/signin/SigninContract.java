package com.maapza.maapza.ui.login.signin;


import com.google.firebase.auth.FirebaseUser;

public interface SigninContract {

    interface View {
        void showMessage(String message);

        void showErrorMessage();

        void showLoading(Boolean show);

        void closeActivity();

        void navToMainActivity();

        void navToVerifyPhoneActivity(String phone);
    }

    interface Presenter {
        void checkSession();

    }

    interface Interactor {
        interface OnFinishedListener {
            void onFinished(FirebaseUser user);
            void onFailure(Throwable t);
        }

        void checkSession(OnFinishedListener onFinishedListener);

    }
}
