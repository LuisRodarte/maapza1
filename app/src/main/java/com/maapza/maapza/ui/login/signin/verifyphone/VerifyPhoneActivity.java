package com.maapza.maapza.ui.login.signin.verifyphone;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.maapza.maapza.MainActivity;
import com.maapza.maapza.R;

import static com.maapza.maapza.common.Constants.BUNDLE_PHONE_NUMBER;

/**
 * Created by LUIS.RODARTE on 11/25/18.
 */

public class VerifyPhoneActivity extends AppCompatActivity implements VerifyPhoneContract.View {


    private EditText editTextCode;
    private VerifyPhoneContract.Presenter presenter;
    private LottieAnimationView progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);
        presenter = new VerifyPhonePresenterImpl(this);
        setUI();

    }

    private void setUI() {

        Typeface font = Typeface.createFromAsset(this.getAssets(), "quicksand.ttf");
        //initializing objects
        progressBar = findViewById(R.id.lottieAnimationView2);
        editTextCode = findViewById(R.id.editTextCode);
        TextView tvWaitCode = findViewById(R.id.tvWaitCode);
        Button buttonSignIn = findViewById(R.id.buttonSignIn);
        editTextCode.setTypeface(font);
        tvWaitCode.setTypeface(font);
        buttonSignIn.setTypeface(font);

        //getting mobile number from the previous activity
        //and sending the verification code to the number
        Intent intent = getIntent();
        String phoneNumber = intent.getStringExtra(BUNDLE_PHONE_NUMBER);
        presenter.verifyPhoneNumber(this, phoneNumber, getCountryZipCode());

        //if the automatic sms detection did not work, user can also enter the code manually
        //so adding a click listener to the button
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = editTextCode.getText().toString().trim();
                if (code.isEmpty() || code.length() < 6) {
                    editTextCode.setError(getApplicationContext().getString(R.string.invalid_code));
                    editTextCode.requestFocus();
                    return;
                }
                //verifying the code entered manually
                presenter.verifyVerificationCode(code);
            }
        });


    }

    public String getCountryZipCode() {
        String CountryID = "";
        String CountryZipCode = "";

        TelephonyManager manager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        //getNetworkCountryIso
        CountryID = manager.getSimCountryIso().toUpperCase();
        String[] rl = this.getResources().getStringArray(R.array.CountryCodes);
        for (int i = 0; i < rl.length; i++) {
            String[] g = rl[i].split(",");
            if (g[1].trim().equals(CountryID.trim())) {
                CountryZipCode = g[0];
                break;
            }
        }
        return CountryZipCode;
    }


    @Override
    public void showMessage(String message) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
        snackbar.setAction("Dismiss", new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        snackbar.show();
    }

    @Override
    public void showErrorMessage() {
        editTextCode.setError(getApplicationContext().getString(R.string.invalid_code));
        editTextCode.requestFocus();
    }

    @Override
    public void showLoading(Boolean show) {
        int stateLoader = show ? View.VISIBLE : View.INVISIBLE;
        progressBar.setVisibility(stateLoader);
    }

    @Override
    public void closeActivity() {
        finish();

    }

    @Override
    public void completeCode(String code) {
        editTextCode.setText(code);
    }

    @Override
    public void navToMainActivity() {
        //verification successful we will start the profile activity
        Intent intent = new Intent(VerifyPhoneActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);

    }
}