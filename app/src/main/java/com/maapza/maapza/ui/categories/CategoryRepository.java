package com.maapza.maapza.ui.categories;

import android.content.Context;
import android.widget.Filter;

import com.maapza.maapza.data.remote.model.CategorySuggestion;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MacHD on 7/16/18.
 */

public class CategoryRepository {
   /* private static   List<CategorySuggestion> categorySuggestions= new ArrayList<>(Arrays.asList(
            new CategorySuggestion("1","Restaurante", "https://firebasestorage.googleapis.com/v0/b/maapza-d8f1e.appspot.com/o/categoriesimages%2Fcoffeexhdpi.png?alt=media&token=f67d8cf4-d1bc-4db3-9b6d-26f5de815f0d"),
            new CategorySuggestion("2","Bares", "https://firebasestorage.googleapis.com/v0/b/maapza-d8f1e.appspot.com/o/categoriesimages%2Fcoffeexhdpi.png?alt=media&token=f67d8cf4-d1bc-4db3-9b6d-26f5de815f0d")));
    ;*/
   private static   List<CategorySuggestion> categorySuggestions;
    public CategoryRepository(List<CategorySuggestion> categorySuggestions) {
        this.categorySuggestions=categorySuggestions;
    }

    private static List<CategorySuggestion> sCategoryWrappers = new ArrayList<>();
    public interface OnFindColorsListener {
        void onResults(List<CategorySuggestion> results);
    }

    public interface OnFindSuggestionsListener {
        void onResults(List<CategorySuggestion> results);
    }


    public static List<CategorySuggestion> getHistory(Context context, int count) {

        List<CategorySuggestion> suggestionList = new ArrayList<>();
        CategorySuggestion catSuggestion;
        for (int i = 0; i < categorySuggestions.size(); i++) {
            catSuggestion = categorySuggestions.get(i);
            suggestionList.add(catSuggestion);
            if (suggestionList.size() == count) {
                break;
            }
        }
        return suggestionList;
    }

    public static void findSuggestions(Context context, String query, final int limit, final long simulatedDelay,
                                       final OnFindSuggestionsListener listener) {
        new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {

                try {
                    Thread.sleep(simulatedDelay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                List<CategorySuggestion> suggestionList = new ArrayList<>();
                if (!(constraint == null || constraint.length() == 0)) {

                    for (CategorySuggestion suggestion : categorySuggestions) {
                        if (suggestion.getBody().toUpperCase()
                                .startsWith(constraint.toString().toUpperCase())) {

                            suggestionList.add(suggestion);
                            if (limit != -1 && suggestionList.size() == limit) {
                                break;
                            }
                        }
                    }
                }

                FilterResults results = new FilterResults();
               /* Collections.sort(suggestionList, new Comparator<CategorySuggestion>() {
                    @Override
                    public int compare(CategorySuggestion lhs, CategorySuggestion rhs) {
                        return lhs.getIsHistory() ? -1 : 0;
                    }
                });*/
                results.values = suggestionList;
                results.count = suggestionList.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                if (listener != null) {
                    listener.onResults((List<CategorySuggestion>) results.values);
                }
            }
        }.filter(query);

    }


    public static void findColors(Context context, String query, final OnFindColorsListener listener) {
        new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {


                List<CategorySuggestion> suggestionList = new ArrayList<>();

                if (!(constraint == null || constraint.length() == 0)) {

                    for (CategorySuggestion categorySuggestion : sCategoryWrappers) {
                        if (categorySuggestion.getBody().toUpperCase()
                                .startsWith(constraint.toString().toUpperCase())) {

                            suggestionList.add(categorySuggestion);
                        }
                    }

                }

                FilterResults results = new FilterResults();
                results.values = suggestionList;
                results.count = suggestionList.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

                if (listener != null) {
                    listener.onResults((List<CategorySuggestion>) results.values);
                }
            }
        }.filter(query);

    }
}
