package com.maapza.maapza.ui.categories;

import com.maapza.maapza.data.remote.model.Category;

import java.util.List;

public class CategoriesPresenterImpl implements CategoriesContract.Presenter, CategoriesContract.Interactor.OnFinishedListener {

    private CategoriesContract.View view;
    private CategoriesContract.Interactor interactor;

    public CategoriesPresenterImpl(CategoriesContract.View view) {
        this.view = view;
        interactor = new CategoriesIteractorimpl();
    }

    @Override
    public void getListCategories() {
        view.showLoading(true);
        interactor.doGetCategories(this);

    }

    @Override
    public void onFinishedGetCategories(List<Category> categories) {
        view.showLoading(false);
        view.displayListCategories(categories);
    }

    @Override
    public void onErrorGetCategories() {
        view.showLoading(false);
        view.showErrorMessage();
        view.closeActivity();
    }

    @Override
    public void onFailure(Throwable t) {
        view.showLoading(false);
        view.showErrorMessage();
        view.closeActivity();
    }

}
