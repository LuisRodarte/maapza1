package com.maapza.maapza.ui.listplaces;

import android.content.Context;
import android.graphics.Typeface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.maapza.maapza.R;
import com.maapza.maapza.common.Constants;
import com.maapza.maapza.data.remote.model.Place;

import java.util.List;

import static com.maapza.maapza.common.Constants.VIEW_TYPE_CELL;


/**
 * Created by MacHD on 1/15/18.
 */

public class PlaceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mcontext;
    private List<Place> placesList;
    private String type;
    private ClickHandler clickHandler;

    public PlaceAdapter(Context mcontext, List<Place> placesList, String type, ClickHandler clickHandler) {
        this.mcontext = mcontext;
        this.placesList = placesList;
        this.type = type;
        this.clickHandler = clickHandler;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_CELL) {
            if (type.equals("all")) {
                View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_all_places, parent, false);
                return new PlaceViewHolder(layoutView);
            } else {
                View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_places, parent, false);
                return new PlaceViewHolder(layoutView);
            }
        } else {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_button_show_all, parent, false);
            return new FooterViewHolder(layoutView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == placesList.size()) ? Constants.VIEW_TYPE_FOOTER : VIEW_TYPE_CELL;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PlaceViewHolder) {
            PlaceViewHolder placeHolder = (PlaceViewHolder) holder;
            Place place = placesList.get(position);
            RequestOptions requestOptions = new RequestOptions();
            requestOptions.placeholder(R.drawable.alpha_gradient);
            requestOptions.error(R.drawable.image_broken);
            requestOptions.centerCrop();
            requestOptions.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
            requestOptions.override(250, 150);
            Glide.with(mcontext)
                    .load(place.getMain_photo())
                    .apply(requestOptions)
                    .into(placeHolder.photo);
            Typeface face;
            face = Typeface.createFromAsset(mcontext.getAssets(), "quicksand.ttf");
            placeHolder.name.setTypeface(face);
            placeHolder.description.setTypeface(face);
            placeHolder.name.setText(place.getName());
            placeHolder.description.setText(place.getDescription());
            placeHolder.rating.setRating(place.getAverage_rating());
        } else if (holder instanceof FooterViewHolder) {
            FooterViewHolder footerHolder = (FooterViewHolder) holder;
            if (type.equals("all"))
                footerHolder.button.setVisibility(View.GONE);
            else
                footerHolder.button.setVisibility(View.VISIBLE);
            footerHolder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickHandler.navToListPlaces();
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return placesList.size() + 1;
    }

    class PlaceViewHolder extends RecyclerView.ViewHolder {
        ImageView photo;
        TextView name;
        TextView description;
        RatingBar rating;
        CardView card;
        ConstraintLayout container;


        public PlaceViewHolder(View itemView) {
            super(itemView);
            photo = itemView.findViewById(R.id.place_photo);
            name = itemView.findViewById(R.id.place_name);
            description = itemView.findViewById(R.id.place_description);
            rating = itemView.findViewById(R.id.place_rating);
            card = itemView.findViewById(R.id.cardView);
            container = itemView.findViewById(R.id.container);
            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickHandler.onClickPlace(placesList.get(getAdapterPosition()).getId_place());
                }
            });
            card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickHandler.onClickPlace(placesList.get(getAdapterPosition()).getId_place());
                }
            });
        }
    }


    class FooterViewHolder extends RecyclerView.ViewHolder {
        Button button;

        public FooterViewHolder(View itemView) {
            super(itemView);
            button = itemView.findViewById(R.id.btnShowAllPlaces);
        }
    }

    public interface ClickHandler {
        void onClickPlace(String idPlace);

        void navToListPlaces();
    }
}
