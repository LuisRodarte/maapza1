package com.maapza.maapza.ui.map;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.maapza.maapza.data.remote.model.CategorySuggestion;
import com.maapza.maapza.data.remote.model.Place;

import java.util.ArrayList;
import java.util.List;

import static com.maapza.maapza.data.remote.FirebaseEndPoints.CATEGORY_ENDPOINT;
import static com.maapza.maapza.data.remote.FirebaseEndPoints.PLACE_ENDPOINT;

public class MapInteractorImp implements MapContract.Interactor {
    private FirebaseDatabase database;

    public MapInteractorImp() {
        database = FirebaseDatabase.getInstance();
    }

    @Override
    public void doGetCategories(final MapContract.Interactor.OnFinishedListener onFinishedListener) {
        final List<CategorySuggestion> suggestions = new ArrayList<>();
        DatabaseReference category = database.getReference(CATEGORY_ENDPOINT);
        category.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                    CategorySuggestion category = categorySnapshot.getValue(CategorySuggestion.class);
                    suggestions.add(category);

                }

                onFinishedListener.onFinishedGetCategories(suggestions);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
              //  onFinishedListener.onErrorGetData();
            }
        });
    }

    @Override
    public void doGetPlaceById(final MapContract.Interactor.OnFinishedListener onFinishedListener, String place_id) {

        DatabaseReference place = database.getReference(PLACE_ENDPOINT).child(place_id);
        place.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Place place = dataSnapshot.getValue(Place.class);
                    onFinishedListener.onFinishedGetPlaceById(place);
                } else onFinishedListener.onPlaceNotFound();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
              //  onFinishedListener.onErrorGetData();
            }
        });
    }

    @Override
    public void doGetPlacesByCategories(final MapContract.Interactor.OnFinishedListener onFinishedListener, String id_category) {

        final List<Place> placesList = new ArrayList<>();
        DatabaseReference place = database.getReference(PLACE_ENDPOINT);
        Query query = place.orderByChild("id_category").equalTo(id_category);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                        Place place = categorySnapshot.getValue(Place.class);
                        placesList.add(place);
                    }

                    onFinishedListener.onFinishedGetPlacesByCategory(placesList);
                }else{
                    onFinishedListener.onPlaceNotFound();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            //    onFinishedListener.onPlaceNotFound();
            }
        });
    }

    @Override
    public void doGetAllPlaces(final MapContract.Interactor.OnFinishedListener onFinishedListener) {

    }
}
