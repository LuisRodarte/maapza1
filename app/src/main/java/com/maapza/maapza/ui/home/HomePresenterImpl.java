package com.maapza.maapza.ui.home;

import com.maapza.maapza.data.remote.model.Category;
import com.maapza.maapza.data.remote.model.Place;

import java.util.List;

public class HomePresenterImpl implements HomeContract.Presenter, HomeContract.Interactor.OnFinishedListener {

    private HomeContract.View view;
    private HomeContract.Interactor interactor;

    public HomePresenterImpl(HomeContract.View view) {
        this.view = view;
        interactor = new HomeInteractorImpl();
    }

    @Override
    public void getListCategories() {
        view.showLoading(true);
        interactor.doGetCategories(this);

    }

    @Override
    public void getPlaces() {
        view.showLoading(true);
        interactor.doGetPlaces(this);
    }

    @Override
    public void onFinishedGetCategories(List<Category> categories) {
        view.showLoading(false);
        view.displayListCategories(categories);
    }

    @Override
    public void onFinishedGetPlaces(List<Place> places) {
        view.showLoading(false);
        view.displayListPlaces(places);
    }


    @Override
    public void onErrorGetCategories() {
        view.showLoading(false);
        view.showCategoryErrorMessage();
    }

    @Override
    public void onErrorGetPlaces() {
        view.showLoading(false);
        view.showPlaceErrorMessage();

    }

    @Override
    public void onFailure(Throwable t) {

    }
}
