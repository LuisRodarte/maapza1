package com.maapza.maapza.ui.map;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.maapza.maapza.R;
import com.maapza.maapza.data.remote.model.InfoWindowData;


/**
 * Created by LUIS.RODARTE on 17/09/2018.
 */

public class CustomInfoWindowMap implements GoogleMap.InfoWindowAdapter {

    private Context context;
    private Typeface font;
    private ProgressBar progressBar;

    public CustomInfoWindowMap(Context ctx) {
        context = ctx;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = ((Activity) context).getLayoutInflater()
                .inflate(R.layout.custom_info_window, null);
        font = Typeface.createFromAsset(context.getAssets(), "quicksand.ttf");
        TextView title = view.findViewById(R.id.title);

        progressBar = view.findViewById(R.id.progressBar);
        title.setTypeface(font);
        ImageView img = view.findViewById(R.id.pic);
        RatingBar rating = view.findViewById(R.id.rating);
        title.setText(marker.getTitle());
        InfoWindowData infoWindowData = (InfoWindowData) marker.getTag();
        rating.setRating(infoWindowData.getRating().floatValue());
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.drawable.alpha_gradient);
        requestOptions.error(R.drawable.alpha_gradient4);
        requestOptions.centerCrop();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.AUTOMATIC);
        progressBar.setVisibility(View.VISIBLE);
        Glide.with(context)
                .load(infoWindowData.getImage())
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .apply(requestOptions)
                .into(img);

        return view;
    }
}
