package com.maapza.maapza.ui.categories;

import com.maapza.maapza.data.remote.model.Category;

import java.util.List;

public interface CategoriesContract {

    interface View {
        void showMessage(String message);

        void showErrorMessage();

        void showLoading(Boolean show);

        void closeActivity();

        void displayListCategories(List<Category> categories);
    }

    interface Presenter {
        void getListCategories();
    }

    interface Interactor {

        interface OnFinishedListener {
            void onFinishedGetCategories(List<Category> categories);

            void onErrorGetCategories();

            void onFailure(Throwable t);
        }

        void doGetCategories(CategoriesContract.Interactor.OnFinishedListener onFinishedListener);

    }
}
