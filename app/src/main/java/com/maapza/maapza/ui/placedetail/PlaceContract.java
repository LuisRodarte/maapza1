package com.maapza.maapza.ui.placedetail;

import android.net.Uri;

import com.maapza.maapza.data.remote.model.Comment;
import com.maapza.maapza.data.remote.model.Place;
import com.maapza.maapza.data.remote.model.PlacePhotos;
import com.maapza.maapza.data.remote.model.Times;
import com.maapza.maapza.data.remote.model.User;
import com.maapza.maapza.ui.profile.ProfileContract;

import java.util.List;

public interface PlaceContract {

    interface View {

        void showErrorMessage();

        void showErrorSendingReview();

        void showReviewSuccess();

        void showLoading(Boolean show);

        void closeActivity();

        void displayPlaceData(Place place);

        void displayTimes(List<Times> timesList);

        void displayComments(List<Comment> listComments);

        void displayPhotos(List<PlacePhotos> listPhotos);
    }

    interface Presenter {

        void getPlacesbyId(String place_id);

        void getPhotosbyIdPlace(String place_id);

        void getTimesByIdPlace(String place_id);

        void getCommentsByIdPlace(String place_id);

        void sendReview(String place_id, String comments, Float rating, String date);

    }

    interface Interactor {

        interface OnFinishedListener {

            void onFinishedGetPlaces(Place place);

            void onFinishedGetPhotos(List<PlacePhotos> listPhotos);

            void onFinishedGetTimes(List<Times> timesList);

            void onFinishedGetComments(List<Comment> listComments);

            void onErrorGetData();

            void onFinishedSendReview();

            void onErrorSendReview();
        }

        void doGetPlacesbyId(PlaceContract.Interactor.OnFinishedListener onFinishedListener, String place_id);

        void doGetPhotosbyIdPlace(PlaceContract.Interactor.OnFinishedListener onFinishedListener, String place_id);

        void doGetTimesByIdPlace(PlaceContract.Interactor.OnFinishedListener onFinishedListener, String place_id);

        void doGetCommentsByIdPlace(PlaceContract.Interactor.OnFinishedListener onFinishedListener, String place_id);

        void doSendReview(PlaceContract.Interactor.OnFinishedListener onFinishedListener, String place_id, String comments, Float rating, String date);


    }
}
