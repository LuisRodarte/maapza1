package com.maapza.maapza.ui.login.signin.verifyphone;

import android.app.Activity;
import android.content.Context;

import com.maapza.maapza.ui.login.signin.SigninContract;

public interface VerifyPhoneContract {

    interface View {

        void showMessage(String message);

        void showErrorMessage();

        void showLoading(Boolean show);

        void closeActivity();

        void completeCode(String code);

        void navToMainActivity();

    }

    interface Presenter {
        void verifyPhoneNumber(Activity activity, String phoneNumber,String zipCode);
        void verifyVerificationCode(String code);

    }

    interface Interactor {
        interface OnFinishedListener {
            void onVerifySuccess();
            void onVerifyError();
            void sendCode(String code);
        }

        void verifyPhoneNumber(Activity activity, String phoneNumber, String zipCode, VerifyPhoneContract.Interactor.OnFinishedListener onFinishedListener);
        void verifyVerificationCode(String code, VerifyPhoneContract.Interactor.OnFinishedListener onFinishedListener);

    }
}
