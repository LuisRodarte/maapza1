package com.maapza.maapza.ui.profile;

import android.net.Uri;

import com.maapza.maapza.data.remote.model.User;

public interface ProfileContract {

    interface View {

        void showErrorGetUserinfo();

        void showErrorUpdatingUserinfo();

        void showLoading(Boolean show);

        void closeActivity();

        void displayUserInfo(User user, Uri photoUrl);

        void showMessageUpdateSuccess();

        void showErrorUpdatingPhoto();

        void showMessageUpdatingPhotoSuccess();

        void navToLogin();
    }

    interface Presenter {
        void getUserInfo();

        void validateSession();

        void signOut();

        void updateUserName(String name);

        void updateUserPhoto(Uri uriProfileImage);
    }

    interface Interactor {

        interface OnFinishedListener {
            void onFinishedSignout();

            void onFinishedGetSession(Boolean session);

            void onFinishedGetUserInfo(User user, Uri photoUrl);

            void onErrorGetUserInfo();

            void onFinishedUpdateUser();

            void onErrorUpdateUser();

            void onFinishedUpdatePhoto();

            void onErrorUpdatePhoto();

            void onFailure(Throwable t);
        }

        void doGetUserInfo(ProfileContract.Interactor.OnFinishedListener onFinishedListener);

        void doGetSession(ProfileContract.Interactor.OnFinishedListener onFinishedListener);

        void doSignOut(ProfileContract.Interactor.OnFinishedListener onFinishedListener);

        void doUpdateName(ProfileContract.Interactor.OnFinishedListener onFinishedListener, String name);

        void doUpdatePhoto(ProfileContract.Interactor.OnFinishedListener onFinishedListener, Uri photoUrl);

    }
}
