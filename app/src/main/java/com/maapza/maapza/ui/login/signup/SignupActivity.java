package com.maapza.maapza.ui.login.signup;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.maapza.maapza.MainActivity;
import com.maapza.maapza.R;
import com.maapza.maapza.ui.login.signin.SigninActivity;

/**
 * Created by MacHD on 2/10/18.
 */

public class SignupActivity extends AppCompatActivity implements View.OnClickListener, SignupContract.View {

    private EditText editTextEmail, editTextPassword, editTextUserName;
    private ProgressBar progressBar;
    private SignupContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singup);
        presenter = new SignupPresenterImpl(this);
        setUI();
    }

    private void setUI() {

        editTextEmail = findViewById(R.id.editTextEmail);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextUserName = findViewById(R.id.editTextUserName);
        TextView textViewLogin = findViewById(R.id.textViewLogin);

        Button btnSingup = findViewById(R.id.buttonSignUp);
        progressBar = findViewById(R.id.progressbar);
        findViewById(R.id.textViewLogin).setOnClickListener(this);
        Typeface font = Typeface.createFromAsset(this.getAssets(), "quicksand.ttf");
        editTextEmail.setTypeface(font);
        editTextPassword.setTypeface(font);
        editTextUserName.setTypeface(font);
        btnSingup.setTypeface(font);
        textViewLogin.setTypeface(font);
        btnSingup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });

        TranslateAnimation anim = new TranslateAnimation(0, 0, 0, 120);
        anim.setDuration(2500);
        anim.setFillAfter(true);
        // Start animating the image
        final ImageView splash = findViewById(R.id.imgPerson);
        splash.startAnimation(anim);
    }

    private void registerUser() {

        final String email = editTextEmail.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();
        final String username = editTextUserName.getText().toString().trim();
        if (email.isEmpty()) {
            editTextEmail.setError(getString(R.string.email_required));
            editTextEmail.requestFocus();
            return;
        }
        if (username.isEmpty()) {
            editTextUserName.setError(getString(R.string.name_required));
            editTextUserName.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            editTextEmail.setError(getString(R.string.invalid_email));
            editTextEmail.requestFocus();
            return;
        }
        if (password.isEmpty()) {
            editTextPassword.setError(getString(R.string.password_required));
            editTextPassword.requestFocus();
            return;
        }
        if (password.length() < 6) {
            editTextPassword.setError(getString(R.string.password_format_error));
            editTextPassword.requestFocus();
            return;
        }

        presenter.registerUser(email, password, username);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonSignUp:
                registerUser();
                break;

            case R.id.textViewLogin:
                finish();
                startActivity(new Intent(this, SigninActivity.class));
                break;
        }

    }

    @Override
    public void showSuccessMessage() {
        Toast.makeText(getApplicationContext(), getString(R.string.user_registered_success),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showErrorUserRegisteredMessage() {
        Toast.makeText(getApplicationContext(), getString(R.string.user_is_registered),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showErrorMessage(String msg) {
        Toast.makeText(getApplicationContext(), msg,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(Boolean show) {
        int stateLoader = show ? View.VISIBLE : View.INVISIBLE;
        progressBar.setVisibility(stateLoader);
    }

    @Override
    public void closeActivity() {
        finish();
    }

    @Override
    public void navToMainActivity() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}