package com.maapza.maapza.ui.map;

import com.maapza.maapza.data.remote.model.Category;
import com.maapza.maapza.data.remote.model.CategorySuggestion;
import com.maapza.maapza.data.remote.model.Comment;
import com.maapza.maapza.data.remote.model.Place;
import com.maapza.maapza.data.remote.model.PlacePhotos;
import com.maapza.maapza.data.remote.model.Times;
import com.maapza.maapza.ui.placedetail.PlaceContract;

import java.util.List;

public interface MapContract {

    interface View {

        void showErrorMessage();

        void showMessageNoPlaceFound();

        void showLoading(Boolean show);

        void closeActivity();

        void displayPlaceData(Place place);

        void displayPlaces(List<Place> placeList);

        void displayCategories(List<CategorySuggestion> suggestions);

    }

    interface Presenter {

        void getCategories();

        void getPlacesById(String place_id);

        void getPlacesByCategory(String id_category);

        void getAllPlaces();
    }

    interface Interactor {

        interface OnFinishedListener {

            void onFinishedGetCategories(List<CategorySuggestion> suggestions);

            void onFinishedGetPlaceById(Place place);

            void onFinishedGetPlacesByCategory(List<Place> placeList);

            void onErrorGetData();

            void onPlaceNotFound();
        }

        void doGetCategories(MapContract.Interactor.OnFinishedListener onFinishedListener);

        void doGetPlaceById(MapContract.Interactor.OnFinishedListener onFinishedListener, String place_id);

        void doGetPlacesByCategories(MapContract.Interactor.OnFinishedListener onFinishedListener, String id_category);

        void doGetAllPlaces(MapContract.Interactor.OnFinishedListener onFinishedListener);

    }
}
