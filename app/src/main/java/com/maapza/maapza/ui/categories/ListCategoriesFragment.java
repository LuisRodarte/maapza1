package com.maapza.maapza.ui.categories;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.maapza.maapza.R;
import com.maapza.maapza.data.remote.model.Category;
import com.maapza.maapza.ui.map.MapFragment;

import java.util.List;

import static com.maapza.maapza.common.Constants.BUNDLE_ACTION;
import static com.maapza.maapza.common.Constants.BUNDLE_ACTION_CATEGORY;
import static com.maapza.maapza.common.Constants.BUNDLE_ID_CATEGORY;
import static com.maapza.maapza.common.Constants.BUNDLE_NAME_CATEGORY;

/**
 * Created by MacHD on 11/4/18.
 */

public class ListCategoriesFragment extends Fragment implements CategoriesContract.View, CategoriesAdapter.ClickHandler {
    private View myFragmentView;
    private RecyclerView recyclerViewCateg;
    private ProgressBar progressBar;

    public static ListCategoriesFragment newInstance() {
        ListCategoriesFragment fragment = new ListCategoriesFragment();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_list_categories, container, false);
        CategoriesContract.Presenter presenter = new CategoriesPresenterImpl(this);
        setUI();
        presenter.getListCategories();
        return myFragmentView;

    }

    private void setUI() {
        recyclerViewCateg = myFragmentView.findViewById(R.id.rvCategories);
        progressBar = myFragmentView.findViewById(R.id.progressbar);
        TextView tvTitle = myFragmentView.findViewById(R.id.tvTitle);
        Typeface face;
        face = Typeface.createFromAsset(getContext().getAssets(), "quicksand.ttf");
        tvTitle.setTypeface(face);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        recyclerViewCateg.setLayoutManager(gridLayoutManager);
    }

    @Override
    public void OnclickItem(String idCategory, String name) {

        Bundle bundle = new Bundle();
        bundle.putString(BUNDLE_ID_CATEGORY, idCategory);
        bundle.putString(BUNDLE_NAME_CATEGORY, name);
        bundle.putString(BUNDLE_ACTION, BUNDLE_ACTION_CATEGORY);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MapFragment mapFragment = new MapFragment();
        mapFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.frame_layout, mapFragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack(mapFragment.getTag());
        fragmentTransaction.commit();

    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(getContext(), getString(R.string.sesion_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading(Boolean show) {
        int stateLoader = show ? View.VISIBLE : View.GONE;
        progressBar.setVisibility(stateLoader);
    }

    @Override
    public void closeActivity() {
        getActivity().finish();
    }

    @Override
    public void displayListCategories(List<Category> categories) {
        CategoriesAdapter adapterCateg = new CategoriesAdapter(getContext(), categories, 2, this);
        recyclerViewCateg.setAdapter(adapterCateg);
    }
}
