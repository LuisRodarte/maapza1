package com.maapza.maapza.ui.listplaces;

import com.maapza.maapza.data.remote.model.Place;

import java.util.List;

public class ListPlacesPresenterImpl implements ListPlacesContract.Presenter, ListPlacesContract.Interactor.OnFinishedListener {

    private ListPlacesContract.View view;
    private ListPlacesContract.Interactor interactor;

    public ListPlacesPresenterImpl(ListPlacesContract.View view) {
        this.view = view;
        interactor = new ListPlacesInteractorImpl();
    }

    @Override
    public void getListPlaces() {
        view.showLoading(true);
        interactor.doGetPlaces(this);

    }

    @Override
    public void onFinishedGetPlaces(List<Place> placeList) {
        view.showLoading(false);
        view.displayListPlaces(placeList);

    }

    @Override
    public void onErrorGetPlaces() {
        view.showLoading(false);
        view.showErrorGetPlaces();
    }

    @Override
    public void onFailure(Throwable t) {
        view.showLoading(false);
        view.showErrorGetPlaces();
    }
}
